int ballcount = 300; 
int ballsize = 9;
float ballspd = 0.45;
float frc = 0.998;
float[]xspd = new float[ballcount];
float[]yspd = new float[ballcount];
float[]xpos = new float[ballcount];
float[]ypos = new float[ballcount];
float[]wdth = new float[ballcount];
float[]hit = new float[ballcount];
float m = millis();
void setup() {
   size(window.innerWidth, window.innerHeight);  // size always goes first!

  background(255);
  frameRate(30);
  smooth();

  for (int i = 0; i<ballcount; i++) {
    xspd[i] = random(-ballspd, ballspd);
    yspd[i] = random(-ballspd, ballspd);

    wdth[i] = random(0.4*ballsize, ballsize);
    hit[i] = wdth[i];

    xpos[i] = random(10, width*0.99);
    ypos[i] = random(10, height*0.99);
  }
}

void draw() {
   size(window.innerWidth, window.innerHeight);
  background(255);
  for (int i=0; i <ballcount; i++) {
    noStroke();
    fill(random(0, 60), random(155, 255), random(200, 255), random(80, 155));
    ellipse(xpos[i], ypos[i], wdth[i], hit[i]);

    if (xpos[i]+wdth[i]/2 >= width || xpos[i]<= wdth[i]/2) {
      xspd[i]*=-1;
    }
    if (ypos[i]+hit[i]/2 >= height || ypos[i]<= hit[i]/2) {
      yspd[i]*=-1;
    }

    for (int k = 0; k<ballcount; k++) {
      float pardis = dist(xpos[i], ypos[i], xpos[k], ypos[k]);
      if (pardis < 20) {
        xspd[i] += (xpos[i] - xpos[k])*0.0005;
        yspd[i] += (ypos[i] - ypos[k])*0.0005;
      }
      if (pardis > 75 && pardis < 21) {
        xspd[i] += (xpos[i] + xpos[k])*0.05;
        yspd[i] += (ypos[i] + ypos[k])*0.05;
      }
    }
    xpos[i]+= xspd[i] + (random(-1, 1)*(0.8*noise(m*0.1)));
    ypos[i]+= yspd[i] + (random(-1, 1)*(0.8*noise(m*0.1)));  
    xspd[i] *= frc;
    yspd[i] *= frc;
    for (int j = 0; j< i; j++) {
      float pdis = dist(xpos[i], ypos[i], xpos[j], ypos[j]);
      if(width < 640){
if (pdis < 15 && pdis >= 6) {
        strokeWeight(1.1);
        stroke(random(0, 60), random(155, 255), random(200, 255), 65);
        line(xpos[i], ypos[i], xpos[j], ypos[j]);
      }
      if (pdis < 5 && pdis > 2) {
        strokeWeight(1.1);
        stroke(random(0, 60), random(155, 255), random(200, 255), 200);
        line(xpos[i], ypos[i], xpos[j], ypos[j]);
      }
}
      if (pdis < 55 && pdis >= 31) {
        strokeWeight(1.1);
        stroke(random(0, 60), random(155, 255), random(200, 255), 65);
        line(xpos[i], ypos[i], xpos[j], ypos[j]);
      }
      if (pdis < 30 && pdis > 5) {
        strokeWeight(1.1);
        stroke(random(0, 60), random(155, 255), random(200, 255), 200);
        line(xpos[i], ypos[i], xpos[j], ypos[j]);
      }
    }
    if (dist(xpos[i], ypos[i], mouseX, mouseY) < 80) {
      xspd[i]-= (xpos[i] - mouseX)*0.002;
      yspd[i]-= (ypos[i] - mouseY)*0.002;
    }
  }
}