int num = 1500;
int num = 550;
float xoff = 0.0;
ArrayList<Par> par;
float m = millis();

void setup(){
    size(window.innerWidth, window.innerHeight);
    background(255);
    smooth();
    par = new ArrayList<Par>();
    for(int i = 0;i<num;i++){
        par.add(new Par());
    }
}

void draw(){
    size(window.innerWidth, window.innerHeight);
    for(Par p: par){
        p.run();
        if(mousePressed == true){
            p.click();
        }
    }
    xoff += 0.009;
    fill(255,255,255,10);
    rect(0,0,width,height);

}

class Par{
    float r;
    color col;
    PVector pos,vec,acc;
    float tpspd;

    Par(){
        r = random(4,12);
        col = color(random(0,25),random(100,200),random(200,255),random(155,200));
        pos = new PVector(random(width),random(height));
        vec = new PVector(0,0);
        acc = new PVector(-0.005,0.005);
        tpspd = 8.5;
    }

    void run(){
        update();
        display();
    }

    void update(){
        PVector mouse = new PVector(mouseX,mouseY);
        PVector dir = new PVector.sub(mouse,pos);
        dir.normalize();
        dir.mult(0.5);
        dir.mult(noise(xoff));
        acc = dir;
        vec.add(acc);
        vec.limit(tpspd);
        pos.add(vec);
        if ((pos.x > width) || (pos.x < 0)) {
            vec.x *= -1;
        }
        if ((pos.y > height) || (pos.y < 0)) {
            vec.y *= -1;
        }
    }
    
    void display(){
        fill(col);
        noStroke();
        ellipse(pos.x,pos.y,r,r);
    }
        void click(){
        PVector mouse = new PVector(mouseX,mouseY);
        PVector dir = new PVector.sub(pos,mouse);
        dir.normalize();
        dir.mult(0.10);
        dir.mult(noise(m));
        dir.mult(1.5);
         acc = dir;
        vec.add(acc);
        pos.add(vec);
    }
}